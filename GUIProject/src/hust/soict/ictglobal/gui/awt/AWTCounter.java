package hust.soict.ictglobal.gui.awt;
import java.awt.*;
import java.awt.event.*;


public class AWTCounter extends Frame implements ActionListener {
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count=0;
	
	
	public AWTCounter() {
		// TODO Auto-generated constructor stub
		setLayout(new FlowLayout());
		lblCount=new Label("Counter");
		add(lblCount);
		tfCount=new TextField(count+"", 10);
		tfCount.setEditable(false);
		add(tfCount);
		
		btnCount=new Button("Count");
		add(btnCount);
		
		btnCount.addActionListener(this);
		addWindowListener( 
	              new java.awt.event.WindowAdapter() {
	                public void windowClosing( java.awt.event.WindowEvent e ) {
	                  System.out.println( "Closing window!" );
	                  dispose() ;
	                  System.exit( 0 );
	                }
	                }
	            );
		
		setTitle("AWT Counter");
		setSize(300,100);
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AWTCounter app = new AWTCounter();
	}
	
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		++count;
		tfCount.setText(count+"");
	}

}
