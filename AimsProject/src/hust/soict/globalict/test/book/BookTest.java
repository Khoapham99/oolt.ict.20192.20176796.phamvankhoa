package hust.soict.globalict.test.book;

import java.util.List;
import java.util.ArrayList;
import hust.soict.globalict.aims.media.Book;

public class BookTest {

	public BookTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book book = new Book("A Book");
		
		book.setCategory("Documentary");
		book.setCost(5);
		List<String> authors = new ArrayList<String>();
		authors.add("Wikipedia");
		book.setAuthors(authors);
		book.setContent("my my name name name is Khoa, I am am 18 years old old. I am living in Hanoi.");
//		System.out.println(book.toString());
		book.printwordFreq();

	}

}
