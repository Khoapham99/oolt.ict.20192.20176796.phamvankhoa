package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder = new Order();
		anOrder.setDateOrdered();
		anOrder.numberOrders();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Harry Potter and the chamber of secrets");
		dvd1.setCategory("Adventure");
		dvd1.setCost(19.95f);
		dvd1.setDirector("J.K.Rowling");
		dvd1.setLength(87);
//		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Harry Potter and the deathly hallow");
		dvd2.setCategory("Adventure");
		dvd2.setCost(24.95f);
		dvd2.setDirector("J.K.Rowling");
		dvd2.setLength(124);
//		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Harry and the Potter");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
//		anOrder.addDigitalVideoDisc(dvd3);
		
		System.out.println(dvd1.search("Harry Potter") + " " +  dvd3.search("Harry Potter"));
		
//		DigitalVideoDisc[] dvdList = {dvd1, dvd2, dvd3};
		anOrder.addDigitalVideoDisc(dvd1, dvd2, dvd3);
		
		anOrder.printList();
		String luckyTitle = anOrder.getALuckyItem().getTitle();
		System.out.println("The lucky disc is:  " + luckyTitle);
		anOrder.printList();
	}

}
