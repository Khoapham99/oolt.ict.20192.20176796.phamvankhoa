package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {

	public static void main(String[] args) throws Exception {
		MyDate currentDate = new MyDate();
		MyDate customDate = new MyDate(27,1,1999);
		MyDate stringDate = new MyDate("April 30th 1975");
		System.out.println("Custom Date: " + customDate.getDay() + " / " + customDate.getMonth() + " / " + customDate.getYear());
		
		stringDate.setDay(29);
		System.out.println("String Date: " + stringDate.getDay() + " / " + stringDate.getMonth() + " / " + stringDate.getYear());
	
		customDate.accept();
		System.out.println("Custom Date: " + customDate.getDay() + " / " + customDate.getMonth() + " / " + customDate.getYear());
		currentDate.print();
		System.out.println("Current Date: " + currentDate.getDay() + " / " + currentDate.getMonth() + " / " + currentDate.getYear());
		DateUtils.dateCompare(currentDate, customDate);
		DateUtils.dateSort(currentDate, customDate, stringDate);
	}

}
