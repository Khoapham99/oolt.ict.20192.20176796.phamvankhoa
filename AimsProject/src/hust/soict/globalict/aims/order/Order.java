package hust.soict.globalict.aims.order;
import hust.soict.globalict.aims.media.*;

import java.util.ArrayList;
import java.util.Date;

import hust.soict.globalict.aims.media.DigitalVideoDisc;

import java.math.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	public String dateOrdered;
	private static int nbOrders = 0;
	public static final int MAX_LIMITTED_ORDERS = 5;

	public void addMedia(Media media) {
		if (itemsOrdered.size() >= MAX_NUMBERS_ORDERED) {
			System.out.println("The list is already full!");
		}
		else {
			itemsOrdered.add(media);
		}
	}
	public void removeMedia(Media media) {
		if(!itemsOrdered.contains(media)) System.out.println(media.getTitle()+" is not in the list!");
		else itemsOrdered.remove(media);
	}
	public Media getMediaFromTitle(String title) {
		for(int i =0;i<itemsOrdered.size();i++) {
			if(title.equals(itemsOrdered.get(i).getTitle())==true) {
				return itemsOrdered.get(i);
			}
		}
		return null;
	}
//	public boolean getMediaFromTitle(String title) {
//		for(int i =0;i<itemsOrdered.size();i++) {
//			System.out.println(title.equals(itemsOrdered.get(i).getTitle()));
//			if(title.equals(itemsOrdered.get(i).getTitle())==true) {
//				return true;
//			}
//		}
//		return false;
//	}
	public String getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered() {
		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	       Date dateobj = new Date();
	       this.dateOrdered=df.format(dateobj);
	}
	public float totalCost() {
		float sum=0;
		for(int i = 0; i < itemsOrdered.size();i++) {
			sum = sum+ itemsOrdered.get(i).getCost();
		}
		return sum;
	}
	public void numberOrders() {
		if(nbOrders >= MAX_LIMITTED_ORDERS) {
			System.out.println("You have reached the maximum orders!");
		}else {
			nbOrders ++;
		}
	}
	public void printList() {
		System.out.println("***************************Order***************************");
		System.out.println("Date: "+getDateOrdered());
		System.out.println("Ordered Items: ");
		for(int i = 0; i<itemsOrdered.size();i++) {
			if(itemsOrdered.get(i) instanceof DigitalVideoDisc) {
				System.out.println(i+". DVD - "+itemsOrdered.get(i).getTitle()+" - "+itemsOrdered.get(i).getCategory()
						+": "+itemsOrdered.get(i).getCost()+"$");
			}
			if(itemsOrdered.get(i) instanceof Book) {
				System.out.println(i+". Book - "+itemsOrdered.get(i).getTitle()+" - "+itemsOrdered.get(i).getCategory()
						+": "+itemsOrdered.get(i).getCost()+"$");
			}
			if(itemsOrdered.get(i) instanceof CompactDisc) {
				System.out.println(i+". CD - "+itemsOrdered.get(i).getTitle()+" - "+itemsOrdered.get(i).getCategory()
						+": "+itemsOrdered.get(i).getCost()+"$");
			}
		}
		System.out.println("Total cost: "+totalCost());
		System.out.println("***********************************************************");
	}
	public Media getALuckyItem() {
		int randItem = (int)(Math.random() * (itemsOrdered.size()));
		itemsOrdered.get(randItem).setCost(0);
		return itemsOrdered.get(randItem);
	}
}
