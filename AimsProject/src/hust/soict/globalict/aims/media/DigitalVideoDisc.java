package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc>{
	public DigitalVideoDisc(String title, int length) {
		super(title, length);
	}
	public DigitalVideoDisc(String title, String category,float cost, String director, int length) {
		super(title, category, cost);
		this.director=director;
		this.length=length;
	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	public DigitalVideoDisc(String title, String category,float cost) {
		super(title, category, cost);
	}
	private String director;
	private int length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void play() {
		if(this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw(new PlayerException());
		}
		System.out.println("Playing DVD: "+ this.getTitle());
		System.out.println("DVD length: "+this.getLength());
	}
//	public DigitalVideoDisc(String title, String category) {
//		super();
//		this.title = title;
//		this.category = category;
//	}
//	public DigitalVideoDisc(String title, String category, String director) {
//		super();
//		this.title = title;
//		this.category = category;
//		this.director = director;
//	}
//	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
//		super();
//		this.title = title;
//		this.category = category;
//		this.director = director;
//		this.length = length;
//		this.cost = cost;
//	}
	public boolean search(String title) {
		String[] tokens = title.split(" ");
		String searchTitle = this.getTitle();
		if(searchTitle.contains(tokens[0]) && searchTitle.contains(tokens[1])) {
			return true;
		}
		return false;
	}
@Override
public int compareTo(DigitalVideoDisc o) {
	// TODO Auto-generated method stub
	if(getTitle().length()>o.getTitle().length()) return 1;
	else if(getTitle().length()<o.getTitle().length()) return -1;
	else return 0;
}
	
}




























