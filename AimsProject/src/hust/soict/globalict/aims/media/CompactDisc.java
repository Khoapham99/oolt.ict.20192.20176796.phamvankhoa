package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hust.soict.globalict.aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<Track>();
	
	public void setLength(int length) {
		this.length = length;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	public void addTrack(Track track) {
		if(!tracks.contains(track)) {
			tracks.add(track);
		} else System.out.println("The track is already in the list!");
	}
	public void removeTrack(Track track) {
		if(tracks.contains(track)) {
			tracks.remove(track);
		}else System.out.println("The track isnt exist!");
	}
	public int getLength() {
		int totalLength=0;
		for(Track track : tracks) {
			totalLength=totalLength + track.getLength();
		}
		return totalLength;
	}
	public CompactDisc(String title, String category, float cost, int length) {
		super(title, category, cost);
		this.length=length;
	}
	public CompactDisc(String title, int length) {
		super(title, length);
	}
	public CompactDisc(String title) {
		super(title);
	}
	public void play() throws PlayerException{
		if (this.getLength()<=0) {
			System.err.println("ERROR: CD length is 0");
			throw (new PlayerException());
		}
		Iterator<Track> iter = tracks.iterator();
		Track nextTrack;
		
		System.out.println("Playing CD: "+this.getTitle());
		System.out.println("CD length: "+this.getLength());
		while(iter.hasNext()) {
			nextTrack=(Track) iter.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public int compareTo(CompactDisc o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
