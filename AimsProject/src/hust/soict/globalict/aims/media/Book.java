package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable<Book> {
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		if(!authors.contains(authorName)) {
			authors.add(authorName);
		}
	}
	public void removeAuthor(String authorName) {
		if(authors.contains(authorName)) {
			authors.remove(authorName);
		}else System.out.println("Author not on the list!");
	}
	public Book(String title) {
		super(title);
	}
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	public Book(String title, String category,float cost, List<String> authors){
			super(title, category, cost);
			this.authors = authors;
	}
	public void processContent() {
		String[] tokens=content.split(" |\\.|\\,|\\:|\\;|\\'|\\(|\\)");
		Arrays.sort(tokens);
		for(String token : tokens) {
			contentTokens.add(token);
		}
		for(String token:tokens) {
			wordFrequency.put(token, wordFreq(token));
		}
	}
	private int wordFreq(String string) {
		List<String> word = new ArrayList<String>();
		int count=0;
		for(String s : contentTokens) {
			count=0;
			if(word.contains(s)) continue;
			else if(s==string) {
				word.add(s);
				count++;
			}
		}
		return count;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
//	public String printwordFreq() {
//		String print="";
//		for(String word : wordFrequency.keySet()) {
//			String key = word;
//			String value = wordFrequency.get(word).toString();
//			print = key+" "+value+"\n";
//		}
//		return print;
//	}
	public void printwordFreq() {
		processContent();
		
		for(String word : wordFrequency.keySet()) {
			String key = word;
			String value = wordFrequency.get(word).toString();
			System.out.println(key+" "+value);
		}
	}
	@Override
	public int compareTo(Book o) {
		// TODO Auto-generated method stub
		return 0;
	}
//	@Override
//	public String toString() {
//		return "Book [authors=" + authors + ",  wordFrequency=" + printwordFreq() + ", title=" + title + ", getTitle()=" + getTitle()
//				+ ", getCategory()=" + getCategory() + ", getCost()=" + getCost() + "]";
//	}  


}
