package hust.soict.globalict.aims;
import java.util.*;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;

public class Aims extends Thread{
	
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	
	
	

	
	public static void main(String[] args) {
//		Thread thread = new Thread(new MemoryDaemon());
//		thread.setDaemon(true);
//		thread.start();
		List<CompactDisc> CDs = new ArrayList<CompactDisc>();
		List<DigitalVideoDisc> DVDs = new ArrayList<DigitalVideoDisc>();
		List<Book> Books = new ArrayList<Book>();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", 87);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", 124);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin", 90);
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Warmup", 3);
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Scales", 4);
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Introduction", 6);
		DVDs.add(dvd1); DVDs.add(dvd2);DVDs.add(dvd3);DVDs.add(dvd4);DVDs.add(dvd5);DVDs.add(dvd6);
		Iterator<DigitalVideoDisc> iterator = DVDs.iterator();
		System.out.println("------------------------------------");
		System.out.println("The DVDs currently in the order are: ");
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		Collections.sort(DVDs);
		iterator = DVDs.iterator();
		System.out.println("------------------------------------");
		System.out.println("The DVDs in sorted order are: ");
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		System.out.println("------------------------------------");
//		int choice=-1;
//		Order order = null;
//		Scanner sc = new Scanner(System.in);
//		showMenu();
//		do {
//			System.out.print("Input choice: ");
//			choice = sc.nextInt();
//			sc.nextLine();
//			switch(choice) {
//				case 1: 
//					order=new Order();
//					System.out.println("New order created!");
//					break;
//				case 2:
//					String inputString;
//					int inputInt;
//					if(order==null) {
//						System.out.println("No order created!");
//						break;
//					}
//					System.out.print("Add a book(1) or a DVD(2) or a CD(3)?");
//					int inputChoice = sc.nextInt();
//					sc.nextLine();
//					if(inputChoice == 1) {
//						System.out.print("Please input Title: ");
//						inputString = sc.nextLine();
//						Media book = new Book(inputString);
//						order.addMedia(book);
//					}else if(inputChoice==2) {
//						System.out.print("Please input Title: ");
//						inputString = sc.nextLine();
//						DigitalVideoDisc disc = new DigitalVideoDisc(inputString);
//						order.addMedia(disc);
//						System.out.println("Do you want to play this DVD? 1:Yes or 2:No");
//						inputInt = sc.nextInt();
//						sc.nextLine();
//						if(inputInt ==1) disc.play();
//					}else if(inputChoice ==3) {
//						System.out.print("Please input Title: ");
//						inputString = sc.nextLine();
//						CompactDisc disc = new CompactDisc(inputString);
//						System.out.println("Input the number of tracks to add:");
//						int numofTrack = sc.nextInt();
//						sc.nextLine();
//						for(int i =0; i <numofTrack;i++) {
//							Track track = new Track();
//							System.out.println("Please input track title: ");
//							inputString = sc.nextLine();
//							track.setTitle(inputString);
//							System.out.println("Please input track length: ");
//							inputInt = sc.nextInt();
//							sc.nextLine();
//							track.setLength(inputInt);
//							disc.addTrack(track);
//						}
//						order.addMedia(disc);
//						System.out.println("Do you want to play this CD? 1:Yes or 2:No");
//						inputInt=sc.nextInt();
//						if(inputInt ==1 ) {
//							disc.play();
//						}
//					}else System.out.println("Invalid Choice!");
//					break;
//				case 3:	
//					if(order==null) {
//						System.out.println("No order created!");
//						break;
//					}
//					System.out.print("Input the title to delete: ");
//					String title=sc.nextLine();
//					if(order.getMediaFromTitle(title)!=null) {
//						order.removeMedia(order.getMediaFromTitle(title));
//						System.out.println("Deleted!");
//					}else System.out.println("This media is not on the order!");
////					boolean check = order.getMediaFromTitle(title);
////					System.out.println(check);
//					break;
//				case 4: 
//					if(order==null) {
//						System.out.println("No order created!");
//						break;
//					}
//					order.printList();
//					break;	
//				case 0:
//					System.out.println("Exit!");
//					break;
//			}
//		}while(choice!=0);
//		sc.close();
	}
}
