package hust.soict.globalict.aims.utils;
import java.util.Calendar;
import java.util.Scanner;
public class MyDate {
	private int day;
	private int month;
	private int year;
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(day>=1 && day <=31) {
			this.day = day;
		}else {
			System.out.println("Invalid Day!");
		}
	}

	public void setDay(String day) {
		
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}else {
			System.out.println("Invalid Month!");
		}
	}
	public void setMonth(String month){
		this.month = getMonthNumber(month);
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public MyDate() {
		super();
		Calendar calendar = Calendar.getInstance();  
		this.day = calendar.get(Calendar.DATE);
		this.month = calendar.get(Calendar.MONTH)+1;
		this.year = calendar.get(Calendar.YEAR);
	}
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String Date) {
		super();
		String parts[] = Date.split(" ");
		String monthStr = parts[0];
		String dayStr;
//		System.out.println(monthStr);
		this.month = getMonthNumber(monthStr);
//		System.out.println(monthStr);
//		this.month = Integer.parseInt(monthStr);
		dayStr = parts[1];
		dayStr = dayStr.replaceAll("\\D+","");
		this.day = Integer.parseInt(dayStr);
		this.year = Integer.parseInt(parts[2]);
	}
	
	public void accept() {
		System.out.print("Enter a date: ");
        Scanner scanner = new Scanner(System. in);
        String inputDate = scanner. nextLine();
        String parts[]=inputDate.split(" ");
        scanner.close();
		String monthStr = parts[0];
		String dayStr;
//		System.out.println(monthStr);
		this.month=getMonthNumber(monthStr);
		
//		System.out.println(monthStr);
//		this.month = Integer.parseInt(monthStr);
		dayStr = parts[1];
		dayStr = dayStr.replaceAll("\\D+","");
		this.day = Integer.parseInt(dayStr);
		this.year = Integer.parseInt(parts[2]);
	}
	public void print() {
		System.out.print("The current date is: ");
		Calendar calendar = Calendar.getInstance();  
		System.out.println(calendar.get(Calendar.DATE) + " / " + (calendar.get(Calendar.MONTH)+1) + " / " + calendar.get(Calendar.YEAR));
	}
	public String getStringDate() {
		String dateStr;
		dateStr = String.valueOf(getDay())+"/"+String.valueOf(getMonth()) +"/"+ String.valueOf(getYear());
		return dateStr;
	}
	public int getMonthNumber(String monthName) {
		int monthNum=0;
		switch(monthName) {
			case "January":
			case "Jan":
				monthNum = 1;
				break;
			case "February":
			case "Feb":
				monthNum = 2;
				break;
			case "March":
			case "Mar":
				monthNum = 3;
				break;
			case "April":
			case "Apr":
				monthNum = 4;
				break;
			case "May":
				monthNum = 5;
				break;
			case "June":
			case "Jun":
				monthNum = 6;
				break;
			case "July":
			case "Jul":
				monthNum = 7;
				break;
			case "August":
			case "Aug":
				monthNum = 8;
				break;
			case "September":
			case "Sep":
				monthNum = 9;
				break;
			case "October":
			case "Oct":
				monthNum = 10;
				break;
			case "November":
			case "Nov":
				monthNum = 11;
				break;
			case "December":
			case "Dec":
				monthNum = 12;
				break;
		}
		return monthNum;
	}
	
}
