package hust.soict.globalict.lab01;
import javax.swing.JOptionPane;
import java.io.*;
public class ex6{
	static String solveEquation(String equation) 
	{ 
	    int n = equation.length();
	    int num=0,var=0,result=0;
	    int sign=1, j=0;
	    for(int i =0;i<n;i++) {
	    	if(equation.charAt(i)=='+'||equation.charAt(i)=='-') {
	    		if(i>j) {
	    			num+=sign*Integer.parseInt(equation.substring(j, i));
	    		}
	    		j=i;
	    	}
	    	else if(equation.charAt(i)=='x') {
	    		if((j==i)||equation.charAt(i-1)=='+') var+=sign;
	    		else if(equation.charAt(i-1)=='-') var-=sign;
	    		else {
	    			var += sign*Integer.parseInt(equation.substring(j, i));
	    		}
	    		j=i+1;
	    	}
	    	else if(equation.charAt(i)=='=') {
	    		if(i>j) {
	    			num+=sign*Integer.parseInt(equation.substring(j, i));
	    		}
    			sign=-1;
    			j=i+1;
	    	}
	    }
	    if(j<n) {
    		num=num+sign*Integer.parseInt(equation.substring(j));
    	}
	    if(num==0 && var==0) return "Infinite solutions";
	    else if(var==0 && num !=0) return "No solution";
	    result = -num/var;
	    return(Integer.toString(result));
	} 
	public static void main(String[] args) {
		String strEqua;
		strEqua = JOptionPane.showInputDialog(null, "Please input the first degree equation of one variable: ", "Input the equation",JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "The result: "+solveEquation(strEqua),"RESULT",JOptionPane.INFORMATION_MESSAGE);

	}
}