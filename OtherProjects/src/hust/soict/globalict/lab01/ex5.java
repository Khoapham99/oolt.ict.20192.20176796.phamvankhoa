package hust.soict.globalict.lab01;
// 5. Write a program to calculate sum, difference, product, and quotient of 2 double numbers which are entered by users.
import javax.swing.JOptionPane;
import java.lang.Math;
public class ex5{
	public static void main(String[] args) {
		String strNum1, strNum2;
		String strSum = "Sum: ";
		String strDiff = "Difference: ";
		String strProduct = "Product: ";
		String strQuot = "Quotient: ";
		
		strNum1 = JOptionPane.showInputDialog(null,"Please input the first number: ","Input first number",JOptionPane.INFORMATION_MESSAGE);
		strNum2 = JOptionPane.showInputDialog(null,"Please input the second number: ","Input second number",JOptionPane.INFORMATION_MESSAGE);
		
		double num1 = Double.parseDouble(strNum1);
		double num2 = Double.parseDouble(strNum2);
		
		double sum = num1+num2;
		strSum += String.valueOf(sum);
		
		double diff = Math.abs(num1-num2);
		strDiff += String.valueOf(diff);
		
		double product = num1*num2;
		strProduct += String.valueOf(product);
		
		double quot;
		if(num2 != 0) {quot=num1/num2; strQuot += String.valueOf(quot);}
		else {strQuot = "The divisor is invalid!";}
		
		String strResult = strSum + "\n" + strDiff + "\n" + strProduct + "\n" + strQuot;
		JOptionPane.showMessageDialog(null, strResult, "The Results", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}