package hust.soict.globalict.lab02;
import java.time.YearMonth;
import java.util.Scanner;
public class ex5{
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("What's the year?");
		int iYear = keyboard.nextInt();
		System.out.println("What's the month?");
		int iMonth = keyboard.nextInt();
		if(iMonth<1 || iMonth >12) System.out.println("Month invalid!");
		else {
			YearMonth yearMonthObject = YearMonth.of(iYear, iMonth);
			int daysInMonth = yearMonthObject.lengthOfMonth();
			System.out.println("Number of days: "+daysInMonth);
		}
	}
}